const { expect } = require('chai');

const None = require('./index.js');
const Some = require('../some');

describe('None', () => {
  const none = None.of();

  describe('is_some()', () => {
    it('should return false', () => {
      expect(none.is_some()).to.be.false;
    });
  });

  describe('is_none()', () => {
    it('should return true', () => {
      expect(none.is_none()).to.be.true;
    });
  });

  describe('to_string()', () => {
    it('should return "None"', () => {
      const result = none.to_string();

      expect(result).to.equal('None');
    });
  });

  describe('and(optb)', () => {
    it('should return None when optb is None', () => {
      const result = none.and(None.of());

      expect(result.is_none()).to.be.true;
    });

    it('should return None when optb is Some', () => {
      const result = none.and(Some.of(2));

      expect(result.is_none()).to.be.true;
    });
  });

  describe('and_then(fn)', () => {
    it('should return None', () => {
      const result = none.and_then(() => {});

      expect(result.is_none()).to.be.true;
    });
  });

  describe('or(optb)', () => {
    it('should return optb', () => {
      const optb = None.of();
      const result = none.or(optb);

      // Testing for object identity/equality.
      expect(result).to.equal(optb);
    });
  });

  describe('or_else(fn)', () => {
    it('should return the result of fn', () => {
      expect(none.or_else(() => 'a result')).to.equal('a result');
    });
  });

  describe('map(fn)', () => {
    it('should return None', () => {
      const result = none.map(x => x * x);

      expect(result.is_none()).to.be.true;
    });
  });

  describe('map_or(or, fn)', () => {
    it('should return or', () => {
      const result = none.map_or('another value', x => x * x);

      expect(result).to.equal('another value');
    });
  });

  describe('map_or_else(or_fn, fn)', () => {
    it('should return the result of or_fn', () => {
      const or_fn = () => 'another value';
      const result = none.map_or_else(or_fn, x => x * x);

      expect(result).to.equal('another value');
    });
  });

  describe('expect(msg)', () => {
    it('should throw an error with msg as its message', () => {
      const expectant = () => none.expect('An error message');

      expect(expectant).to.throw(Error, 'An error message');
    });
  });

  describe('unwrap()', () => {
    it('should throw an error with "Cannot unwrap None" as its message', () => {
      const expectant = () => none.unwrap();

      expect(expectant).to.throw(Error, 'Cannot unwrap None');
    });
  });

  describe('unwrap_or(or)', () => {
    it('should return or', () => {
      const result = none.unwrap_or('another value');

      expect(result).to.equal('another value');
    });
  });

  describe('unwrap_or_else(or_fn)', () => {
    it('should return the result of or_fn', () => {
      const result = none.unwrap_or_else(() => 'another value');

      expect(result).to.equal('another value');
    });
  });

  describe('filter(fn)', () => {
    it('should return None', () => {
      const result = none.filter(x => x * x);

      expect(result.is_none()).to.be.true;
    });
  });

  describe('match(pattern)', () => {
    it('should call the None branch', () => {
      const result = None.of().match({
        None: () => true,
        Some: () => false
      });

      expect(result).to.be.true;
    })
  });
});
