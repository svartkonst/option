function None() {};

None.prototype.is_some =
None.prototype.isSome =
function () {
  return false;
}

None.prototype.is_none =
None.prototype.isNone  =
function () {
  return true;
}

None.prototype.and = function (_optb) {
  return None.of();
}

None.prototype.and_then =
None.prototype.andThen =
function (_fn) {
  return None.of();
}

None.prototype.or = function (optb) {
  return optb;
}

None.prototype.or_else =
None.prototype.orElse =
function (fn) {
  return fn();
}

None.prototype.map = function (_fn) {
  return None.of();
}

None.prototype.map_or =
None.prototype.mapOr =
function (or, _fn) {
  return or;
}

None.prototype.map_or_else =
None.prototype.mapOrElse =
function (or_fn, _fn) {
  return or_fn();
}

None.prototype.expect = function (msg) {
  throw new Error(msg);
}

None.prototype.unwrap = function () {
  throw new Error('Cannot unwrap None');
}

None.prototype.unwrap_or =
None.prototype.unwrapOr =
function (or) {
  return or;
}

None.prototype.unwrap_or_else =
None.prototype.unwrapOrElse =
function (or_fn) {
  return or_fn();
}

None.prototype.filter = function (_fn) {
  return None.of();
}

None.prototype.to_string =
None.prototype.toString =
function () {
  return 'None';
}

None.prototype.match = function ({ None: whenNone }) {
  return whenNone();
}

None.of = function () {
  return Object.create(new None());
}


module.exports = None;
