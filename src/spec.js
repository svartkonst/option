const { expect, assert } = require('chai');

const { Some, None } = require('./index.js');

describe('Option', () => {

  describe('static', () => {
    describe.skip('is_some/isSome');
    describe.skip('is_none/isNone');
    describe.skip('to_string/toString');
    describe.skip('and');
    describe.skip('and_then/andThen');
    describe.skip('or');
    describe.skip('or_else/orElse');
    describe.skip('map');
    describe.skip('map_or/mapOr');
    describe.skip('map_or_else/mapOrElse');
    describe.skip('expect');
    describe.skip('unwrap');
    describe.skip('unwrap_or/unwrapOr');
    describe.skip('unwrap_or_else/unwrapOrElse');
    describe.skip('filter');
    describe.skip('match');
  });

  describe('Some', () => {
    it('should implement is_some()', () => {
      expect(Some.of().is_some).to.be.a('function');
    });

    it('should implement is_none()', () => {
      expect(Some.of().is_none).to.be.a('function');
    });

    it('should implement and()', () => {
      expect(Some.of().and).to.be.a('function');
    });

    it('should implement and_then()', () => {
      expect(Some.of().and_then).to.be.a('function');
    });

    it('should implement or()', () => {
      expect(Some.of().or).to.be.a('function');
    });

    it('should implement or_else()', () => {
      expect(Some.of().or_else).to.be.a('function');
    });

    it('should implement map()', () => {
      expect(Some.of().map).to.be.a('function');
    });

    it('should implement map_or()', () => {
      expect(Some.of().map_or).to.be.a('function');
    });

    it('should implement map_or_else()', () => {
      expect(Some.of().map_or_else).to.be.a('function');
    });

    it('should implement expect()', () => {
      expect(Some.of().expect).to.be.a('function');
    });

    it('should implement unwrap()', () => {
      expect(Some.of().unwrap).to.be.a('function');
    });

    it('should implement unwrap_or()', () => {
      expect(Some.of().unwrap_or).to.be.a('function');
    });

    it('should implement unwrap_or_else()', () => {
      expect(Some.of().unwrap_or_else).to.be.a('function');
    });

    it('should implement filter()', () => {
      expect(Some.of().filter).to.be.a('function');
    });

    it('should implement match()', () => {
      expect(Some.of().match).to.be.a('function');
    });

    it('should implement all functions as camelCase', () => {
      const some = Some.of();

      assert.exists(some.isSome);
      assert.exists(some.isNone);
      assert.exists(some.toString);
      assert.exists(some.and);
      assert.exists(some.andThen);
      assert.exists(some.or);
      assert.exists(some.orElse);
      assert.exists(some.map);
      assert.exists(some.mapOr);
      assert.exists(some.mapOrElse);
      assert.exists(some.expect);
      assert.exists(some.unwrap);
      assert.exists(some.unwrapOr);
      assert.exists(some.unwrapOrElse);
      assert.exists(some.filter);
      assert.exists(some.match);
    });
  });

  describe('None', () => {
    it('should implement is_some()', () => {
      expect(None.of().is_some).to.be.a('function');
    });

    it('should implement is_none()', () => {
      expect(None.of().is_none).to.be.a('function');
    });

    it('should implement and()', () => {
      expect(None.of().and).to.be.a('function');
    });

    it('should implement and_then()', () => {
      expect(None.of().and_then).to.be.a('function');
    });

    it('should implement or()', () => {
      expect(None.of().or).to.be.a('function');
    });

    it('should implement or_else()', () => {
      expect(None.of().or_else).to.be.a('function');
    });

    it('should implement map()', () => {
      expect(Some.of().map).to.be.a('function');
    });

    it('should implement map_or()', () => {
      expect(Some.of().map_or).to.be.a('function');
    });

    it('should implement map_or_else()', () => {
      expect(Some.of().map_or_else).to.be.a('function');
    });

    it('should implement expect()', () => {
      expect(None.of().expect).to.be.a('function');
    });

    it('should implement unwrap()', () => {
      expect(None.of().unwrap).to.be.a('function');
    });

    it('should implement unwrap_or()', () => {
      expect(None.of().unwrap_or).to.be.a('function');
    });

    it('should implement unwrap_or_else()', () => {
      expect(None.of().unwrap_or_else).to.be.a('function');
    });

    it('should implement filter()', () => {
      expect(None.of().filter).to.be.a('function');
    });

    it('should implement match()', () => {
      expect(Some.of().match).to.be.a('function');
    });

    it('should implement all functions as camelCase', () => {
      const none = None.of();

      assert.exists(none.isSome);
      assert.exists(none.isNone);
      assert.exists(none.toString);
      assert.exists(none.and);
      assert.exists(none.andThen);
      assert.exists(none.or);
      assert.exists(none.orElse);
      assert.exists(none.map);
      assert.exists(none.mapOr);
      assert.exists(none.mapOrElse);
      assert.exists(none.expect);
      assert.exists(none.unwrap);
      assert.exists(none.unwrapOr);
      assert.exists(none.unwrapOrElse);
      assert.exists(none.filter);
      assert.exists(none.match);
    });
  });
});
