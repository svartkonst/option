const None = require('../none');

function Some(value) {
  this.value = value;
}

Some.prototype.is_some =
Some.prototype.isSome =
function () {
  return true;
}

Some.prototype.is_none =
Some.prototype.isNone =
function () {
  return false;
}

Some.prototype.to_string =
Some.prototype.toString =
function () {
  return `Some.of(${this.value})`;
}

Some.prototype.and = function (optb) {
  return optb;
}

Some.prototype.and_then =
Some.prototype.andThen =
function (fn) {
  return fn(this.value);
}

Some.prototype.or = function (_optb) {
  return Some.of(this.value);
}

Some.prototype.or_else =
Some.prototype.orElse =
function (_fn) {
  return Some.of(this.value);
}

Some.prototype.map = function (fn) {
  return Some.of(fn(this.value));
}

Some.prototype.map_or =
Some.prototype.mapOr =
function (_or, fn) {
  return Some.of(fn(this.value));
}

Some.prototype.map_or_else =
Some.prototype.mapOrElse =
function (_or_fn, fn) {
  return Some.of(fn(this.value));
}

Some.prototype.unwrap = function () {
  return this.value;
}

Some.prototype.unwrap_or =
Some.prototype.unwrapOr =
function (_or) {
  return this.unwrap();
}

Some.prototype.unwrap_or_else =
Some.prototype.unwrapOrElse =
function (_or_fn) {
  return this.unwrap();
}

Some.prototype.expect = function (_msg) {
  return this.unwrap();
}

Some.prototype.filter = function (predicate) {
  return predicate(this.value)
    ? Some.of(this.value)
    : None.of();
}


Some.prototype.match = function ({ Some: whenSome }) {
  return whenSome(this.value);
}

Some.of = value => new Some(value);

module.exports = Some;
