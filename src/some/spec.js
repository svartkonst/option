const { expect } = require('chai');

const Some = require('./index.js');
const None = require('../none');

describe('Some', () => {
  const some = Some.of();

  describe('is_some()', () => {
    it('should return true', () => {
      expect(some.is_some()).to.be.true;
    });
  });

  describe('is_none()', () => {
    it('should return false', () => {
      expect(some.is_none()).to.be.false;
    });
  });

  describe('to_string()', () => {
    it('should return "Some.of(x)"', () => {
      const result = Some.of(123).to_string();

      expect(result).to.equal('Some.of(123)');
    });
  });

  describe('and(optb)', () => {
    it('should return optb when optb is None', () => {
      const optb = None.of();
      const result = some.and(optb);

      expect(result.is_none()).to.be.true;
    });

    it('should return optb when optb is Some', () => {
      const optb = Some.of(5);
      const result = some.and(optb);

      expect(result.to_string()).to.equal('Some.of(5)');
    });
  });

  describe('and_then(fn)', () => {
    it('should call fn with the wrapped value', () => {
      let called_with;
      const fn = x => { called_with = x; };

      Some.of(5).and_then(fn);

      expect(called_with).to.equal(5);
    });

    it('should return the result of fn', () => {
      const fn = x =>  Some.of(x * x);

      const result = Some.of(5).and_then(fn);

      expect(result.to_string()).to.equal('Some.of(25)');
    });
  });

  describe('or(optb)', () => {
    it('should return the option', () => {
      const optb = Some.of(7);
      const result = Some.of(5).or(optb);

      expect(result.to_string()).to.equal('Some.of(5)');
    });
  });

  describe('or_else(fn)', () => {
    it('should return the option', () => {
      const optb = Some.of(7);
      const result = Some.of(5).or_else(() => optb);

      expect(result.to_string()).to.equal('Some.of(5)');
    });

    it('should not call fn', () => {
      let called = false;
      const fn = x => { called = true; }

      Some.of(5).or_else(fn);

      expect(called).to.be.false;
    });
  });

  describe('map(fn)', () => {
    it('should return a Some containing the result of fn', () => {
      const result = Some.of(5).map(x => x * x);

      expect(result.to_string()).to.equal('Some.of(25)');
    });
  });

  describe('map_or(or, fn)', () => {
    it('should return a Some containing the result of fn', () => {
      const result = Some.of(5).map_or(-1, x => x * x);

      expect(result.to_string()).to.equal('Some.of(25)');
    });
  });

  describe('map_or_else(or_fn, fn)', () => {
    it('should return a Some containing the result of fn', () => {
      const result = Some.of(5).map_or_else(() => -1, x => x * x);

      expect(result.to_string()).to.equal('Some.of(25)');
    });
  });

  describe('expect(msg)', () => {
    it('should return the value of Some', () => {
      const result = Some.of(5).expect('help');

      expect(result).to.equal(5);
    });
  });

  describe('unwrap()', () => {
    it('should return the value of Some', () => {
      const result = Some.of(5).unwrap();

      expect(result).to.equal(5);
    });
  });

  describe('unwrap_or(or)', () => {
    it('should return the value of Some', () => {
      const result = Some.of(5).unwrap_or('a default');

      expect(result).to.equal(5);
    });
  });

  describe('unwrap_or_else(or_fn)', () => {
    it('should return the value of Some', () => {
      const result = Some.of(5).unwrap_or_else(() => 'a computed default');

      expect(result).to.equal(5);
    });
  });

  describe('filter(predicate_fn)', () => {
    const predicate = x => x > 5;

    it('should return Some if wrapped value passes predicate_fn', () => {
      const result = Some.of(10).filter(predicate);

      expect(result.to_string()).to.equal('Some.of(10)');
    });

    it('should return None if wrapped value fails predicate_fn', () => {
      const result = Some.of(1).filter(predicate);

      expect(result.to_string()).to.equal('None');
    });
  });

  describe('match(pattern)', () => {
    it('should call the Some branch', () => {
      const result = Some.of(10).match({
        None: () => false,
        Some: x => x
      });

      expect(result).to.equal(10);
    })
  });
});
