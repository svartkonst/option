const curry = require('@svartkonst/curry');

const Some = require('../some');
const None = require('../none');

const Option = function () {};

/**
 * Constructors
 */
Option.of     = Option.Some = x => new Some(x);
Option.empty  = Option.None = () => new None();

Option.fromNullable = x => x
    ? new Some(x)
    : new None();

Option.fromEmpty = xs => xs && xs.length
  ? new Some(xs)
  : new None();

/**
 * Static methods.
 *
 * All static methods are curried (similar to lodash)
 * and expect to receive their arguments monad-last, to facilitate easy chaining.
 *
 * Some(x).map(f) becomes Option.map(f, Some(x)), for instance.
 */

Option.is_some =
Option.isSome =
  a => a.is_some();


Option.is_none =
Option.isNone =
  a => a.is_none();

Option.and = curry((b, a) => a.and(b));

Option.and_then =
Option.andThen =
  curry((fn, a) => a.and_then(fn));

Option.or = curry((b, a) => a.or(b));

Option.or_else =
Option.orElse =
  curry((fn, a) => a.or_else(fn));

Option.map = curry((fn, a) => a.map(fn));

Option.map_or =
Option.mapOr =
  curry((or, fn, a) => a.map_or(or, fn));

Option.map_or_else =
Option.mapOrElse =
  curry((or_fn, fn, a) => a.map_or_else(or_fn, fn));

Option.expect = curry((msg, a) => a.expect(msg));

Option.unwrap = a => a.unwrap();

Option.unwrap_or =
Option.unwrapOr =
  curry((or, a) => a.unwrap_or(or));

Option.unwrap_or_else =
Option.unwrapOrElse =
  curry((or_fn, a) => a.unwrap_or_else(or_fn));

Option.filter = curry((fn, a) => a.filter(fn));

Option.to_string =
Option.toString =
  a => a.toString();

Option.match = curry((pattern, a) => a.match(pattern));

module.exports = Option;
