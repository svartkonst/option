module.exports = {
  Option: require('./option'),
  Some: require('./some'),
  None: require('./none')
};
